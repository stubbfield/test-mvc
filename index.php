<?php
require __DIR__ . '/vendor/autoload.php';

use App\Lib\App;
use App\Lib\Router;
use App\Lib\Request;
use App\Lib\Response;
use App\Controller\Home;
use App\Controller\Ajax;

Router::post('/ajax/getOrdersByDate', function (Request $req, Response $res) {
    (new Ajax())->getOrdersByDateAction($req);
});

Router::get('/', function (Request $req) {
   (new Home())->indexAction($req);
});

App::run();