 
$( document ).ready(function() {

    $('.datepick').daterangepicker({
        timePicker: true,
        timePicker24Hour: true,
        timePickerSeconds: true,
        startDate: moment().startOf('hour').subtract(1, 'month'),
        endDate: moment().startOf('hour'),
        locale: {
            format: 'Y-MM-DD HH:mm:ss',
            separator: ">",
        }
    }, function(start, end, label) {
        
    });

    $('.datepick').on('change', function() {
        var val = $(this).val().split(">");

        $.ajax({
            url : '/ajax/getOrdersByDate',
            type: 'POST',
            data : {date_from: val[0], date_to: val[1]},
            success: function(data, textStatus, jqXHR)
            {
                console.log(data.grapthData);
                $('.js-earnings').html(data.earnings);
                $('.js-count').html(data.countOrders);
                $('.js-count-users').html(data.countUsers);

                chart.unload();

                if (data.grapthData) {
                    setTimeout(function() {
                        chart.load( $.parseJSON(data.grapthData) );
                    }, 500);
                }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Sorry, we got some problems, try again later please');
            }
        });
    });

    var chart = c3.generate({
        data: grapthData,
        axis: {
            x: {
                type: "timeseries",
                tick: {
                    format: "%Y-%m-%d"
                }
            }
        }
    });
    
});