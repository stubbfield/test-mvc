
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";



CREATE TABLE `order` (
  `id` int NOT NULL,
  `country` varchar(255) NOT NULL,
  `device` varchar(255) NOT NULL,
  `purchase_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `order_item_id` int NOT NULL,
  `user_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO `order` (`id`, `country`, `device`, `purchase_date`, `order_item_id`, `user_id`) VALUES
(1, 'na', 'mobile', '2020-08-27 01:20:13', 1, 1),
(1, 'na', 'mobile', '2020-08-27 01:20:13', 2, 1),
(1, 'na', 'mobile', '2020-08-27 01:20:13', 4, 1),
(2, 'asd', 'dsa', '2020-08-29 04:58:35', 2, 3),
(3, 'na', 'na', '2020-08-30 02:43:38', 3, 1),
(3, 'na', 'na', '2020-08-30 02:43:38', 4, 3),
(5, 'na', 'na', '2020-08-23 21:04:31', 8, 17),
(5, 'na', 'na', '2020-08-23 21:04:31', 25, 17),
(6, 'qwe', 'wqwe', '2020-08-10 21:04:31', 33, 29),
(7, 'asd', 'asd', '2020-08-31 21:07:40', 10, 8),
(8, 'na', 'na', '2020-08-11 21:04:31', 22, 20),
(9, 'na', 'na', '2020-08-15 21:04:31', 26, 20),
(10, 'qwe', 'wqwe', '2020-08-05 21:04:31', 33, 29),
(11, 'asd', 'asd', '2020-08-31 21:25:29', 10, 8);


CREATE TABLE `order_item` (
  `id` int NOT NULL,
  `ean` bigint NOT NULL COMMENT 'GTN-13 number',
  `quantity` int NOT NULL,
  `price` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO `order_item` (`id`, `ean`, `quantity`, `price`) VALUES
(1, 123456789123, 2, 12.25),
(2, 1134512345631, 1, 12.7),
(3, 1234563456321, 1, 100),
(4, 1234563456322, 21, 1),
(5, 97403, 8, 56.12),
(6, 5879, 4, 91.3),
(7, 957, 3, 7.53),
(8, 57894308, 8, 43.22),
(9, 2, 2, 35.51),
(10, 977784, 7, 67.07),
(11, 7959582, 7, 17.75),
(12, 39889, 8, 78.02),
(13, 148, 9, 79.66),
(14, 3, 6, 8.31),
(15, 4430271, 1, 47.17),
(16, 71707807, 2, 92.67),
(17, 1, 1, 17.82),
(18, 21540513, 7, 38.44),
(19, 611, 1, 62.99),
(20, 65798, 2, 33.52),
(21, 966, 1, 63.54),
(22, 750871497, 1, 44.56),
(23, 5459, 2, 64.92),
(24, 3559, 4, 36.16),
(25, 951, 6, 51.6),
(26, 788078268, 9, 92.39),
(27, 938, 2, 28.82),
(28, 63711, 6, 45.26),
(29, 42, 4, 86.43),
(30, 9035, 7, 50.15),
(31, 376758091, 4, 23.45),
(32, 539, 1, 79.82),
(33, 809088, 1, 95.37),
(34, 247096, 7, 63.12),
(35, 52, 9, 42.2),
(36, 0, 2, 59.63),
(37, 319, 5, 87.84),
(38, 660, 3, 8.15),
(39, 10457, 8, 15.68),
(40, 853240, 5, 99.51),
(41, 585, 6, 62.1),
(42, 81878, 2, 91.36),
(43, 43161002, 7, 77.03),
(44, 55809, 9, 50.14),
(45, 9553, 9, 80.73),
(46, 91754964, 9, 83.34),
(47, 648, 9, 25.89),
(48, 27066307, 9, 56.86),
(49, 4, 2, 9.46),
(50, 961369065, 8, 22.48),
(51, 9920677, 2, 50.6),
(52, 88701716, 7, 51.53),
(53, 4356240, 8, 54.14),
(54, 24355020, 4, 52.86),
(55, 5, 6, 75.92),
(56, 34307, 5, 54.71),
(57, 700599274, 1, 4.57),
(58, 3320101, 3, 25.68),
(59, 8731165, 1, 34.26),
(60, 418492, 5, 92.09),
(61, 27, 6, 43.39),
(62, 770, 1, 88.75),
(63, 259651833, 5, 55.98),
(64, 4948, 9, 68.75),
(65, 190802293, 9, 1.71),
(66, 59, 6, 53.39),
(67, 757, 2, 1.36),
(68, 1917, 7, 22.45),
(69, 74814, 1, 36.4),
(70, 5735, 5, 3.73),
(71, 7289585, 8, 2.06),
(72, 21615, 4, 85.12),
(73, 77, 3, 90.51),
(74, 74047880, 6, 10.78),
(75, 78, 4, 55.3),
(76, 7640, 4, 45.72),
(77, 6, 3, 54.5),
(78, 2860, 4, 90.32),
(79, 294, 1, 15.6),
(80, 5167, 5, 95.3),
(81, 5143000, 4, 46.05),
(82, 889188, 4, 91.83),
(83, 5931, 2, 79.62),
(84, 30169, 9, 8.44),
(85, 23088576, 8, 46.79),
(86, 61917, 4, 69.77),
(87, 66168379, 3, 88.89),
(88, 394, 2, 7.54),
(89, 87155629, 2, 85.01),
(90, 65, 3, 8.56),
(91, 61, 7, 16.6),
(92, 407313, 6, 6.36),
(93, 48339252, 5, 23.58),
(94, 8395, 5, 54.15),
(95, 2933938, 9, 6.87),
(96, 30133297, 6, 32.95),
(97, 732374449, 5, 96.36),
(98, 80609, 3, 18.34),
(99, 5692, 8, 27.85),
(100, 791, 1, 87.51);

CREATE TABLE `user` (
  `id` int NOT NULL,
  `first_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `second_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `user` (`id`, `first_name`, `second_name`, `email`) VALUES
(1, 'Serj', 'Senkin', 'stubbfield@gmail.com'),
(2, 'July', 'Parf', 'sgfhf@gth.jhk'),
(3, 'Tester', 'Test', 'hfghd@fghgf.fgh'),
(4, 'Woodrow', 'Stokes', 'nikita62@example.org'),
(5, 'Bette', 'Hilpert', 'margret70@example.net'),
(6, 'Mikayla', 'Ratke', 'jarret64@example.net'),
(7, 'Myra', 'Kessler', 'ashton67@example.org'),
(8, 'Israel', 'Abshire', 'paige94@example.org'),
(9, 'Kennedy', 'Cruickshank', 'thalia.bailey@example.net'),
(10, 'Odell', 'Rohan', 'abins@example.org'),
(11, 'Rocky', 'Bechtelar', 'burnice.davis@example.org'),
(12, 'Davin', 'Abernathy', 'considine.marian@example.com'),
(13, 'Kattie', 'Gaylord', 'ujohns@example.net'),
(14, 'Clay', 'Dickinson', 'nico12@example.com'),
(15, 'Brannon', 'Hickle', 'dgutmann@example.net'),
(16, 'Rebeka', 'Kiehn', 'awiegand@example.com'),
(17, 'Alf', 'Hermiston', 'jwhite@example.org'),
(18, 'Tobin', 'Turner', 'winston.ziemann@example.net'),
(19, 'Remington', 'O\'Connell', 'cristopher.dickens@example.com'),
(20, 'Percy', 'Harris', 'qpadberg@example.com'),
(21, 'Morris', 'Nitzsche', 'kilback.greyson@example.net'),
(22, 'Shana', 'Wisoky', 'felton87@example.com'),
(23, 'Benny', 'Johns', 'zdavis@example.org'),
(24, 'Gage', 'Waelchi', 'terrill70@example.net'),
(25, 'Alysa', 'Herzog', 'rsmitham@example.org'),
(26, 'Emmalee', 'Mayert', 'russell47@example.net'),
(27, 'Howell', 'Hamill', 'erau@example.org'),
(28, 'Chadd', 'Conn', 'helen59@example.com'),
(29, 'Angel', 'Cremin', 'waino78@example.net'),
(30, 'Cleveland', 'Moore', 'dagmar.herman@example.org');

ALTER TABLE `order`
  ADD PRIMARY KEY (`id`,`order_item_id`),
  ADD KEY `user.id` (`user_id`),
  ADD KEY `order_item.id` (`order_item_id`);

ALTER TABLE `order_item`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `order_item`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;

ALTER TABLE `user`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

ALTER TABLE `order`
  ADD CONSTRAINT `order_item.id` FOREIGN KEY (`order_item_id`) REFERENCES `order_item` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `user.id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
COMMIT;