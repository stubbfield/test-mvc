<?php

namespace App\Model;

use PDO;

/**
 * Example user model
 */
class OrderItem extends \App\Lib\Model
{

    /**
     * Get all the users as an associative array
     *
     * @return array
     */
    public function findAll(array $filter = null)
    {
        if ($filter) {
        }

        $dbSt = $this->db->prepare("SELECT id, first_name, second_name FROM $this->dbOrderItem");
        $dbSt->execute();

        return $dbSt->fetchAll(PDO::FETCH_ASSOC);
    }
}
