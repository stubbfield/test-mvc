<?php

namespace App\Model;

use PDO;

/**
 * Example user model
 */
class Order extends \App\Lib\Model
{
    public function findAllDetail(array $filter = [])
    {
        $whereAr = [];
        $sqlStr = "SELECT os.id, os.order_item_id, os.user_id, 
            CONCAT(u.first_name, ' ', u.second_name) AS user_name, 
            oi.quantity, oi.price, os.purchase_date
            FROM $this->dbOrder os
                INNER JOIN $this->dbOrderItem oi
                    ON oi.id = os.order_item_id
                INNER JOIN $this->dbUser u
                    ON u.id = os.user_id";
        
        if (!empty($filter) && !empty($filter['date_from']) && !empty($filter['date_to'])) {
            $sqlStr .= " WHERE os.purchase_date between :date_from and :date_to";
            $whereAr = [
                'date_from' => date('Y-m-d H:i:s', strtotime($filter['date_from'])),
                'date_to' => date('Y-m-d H:i:s', strtotime($filter['date_to'])),
            ];
        }

        $dbSt = $this->db->prepare($sqlStr);
        $dbSt->execute($whereAr);

        return $dbSt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function countAll(array $filter = [])
    {
        $whereAr = [];
        $sqlStr = "SELECT COUNT(DISTINCT id) As countOrders FROM $this->dbOrder";

        if (!empty($filter) && !empty($filter['date_from']) && !empty($filter['date_to'])) {
            $sqlStr .= " WHERE purchase_date between :date_from and :date_to";
            $whereAr = [
                'date_from' => date('Y-m-d H:i:s', strtotime($filter['date_from'])),
                'date_to' => date('Y-m-d H:i:s', strtotime($filter['date_to'])),
            ];
        }

        $dbSt = $this->db->prepare($sqlStr);

        $dbSt->execute($whereAr);

        return $dbSt->fetch(PDO::FETCH_ASSOC);
    }
}
