<?php

namespace App\Model;

use PDO;

/**
 * Example user model
 */
class User extends \App\Lib\Model
{

    /**
     * Get all the users
     *
     * @return array
     */
    public function findAll(array $filter = [])
    {
        // if ($filter) {}

        $dbSt = $this->db->prepare("SELECT id, first_name, second_name FROM $this->dbUser");
        $dbSt->execute();

        return $dbSt->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Get count of all users
     *
     * @return array
     */
    public function countAll(array $filter = [])
    {
        $whereAr = [];

        $sqlStr = "SELECT COUNT(DISTINCT u.id) as countUsers
        FROM $this->dbUser u
            INNER JOIN $this->dbOrder os
            ON os.user_id = u.id";

        if (!empty($filter) && !empty($filter['date_from']) && !empty($filter['date_to'])) {
            $sqlStr .= " WHERE os.purchase_date between :date_from and :date_to";
            $whereAr = [
                'date_from' => date('Y-m-d H:i:s', strtotime($filter['date_from'])),
                'date_to' => date('Y-m-d H:i:s', strtotime($filter['date_to'])),
            ];
        }
        
        $dbSt = $this->db->prepare($sqlStr);
        
        $dbSt->execute($whereAr);

        return $dbSt->fetch(PDO::FETCH_ASSOC);
    }
}
