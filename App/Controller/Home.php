<?php
namespace App\Controller;

use App\Model\Order;
use App\Model\User;
use App\Lib\Utils;
use App\Lib\Request;

class Home extends \App\Lib\Controller
{
    public function indexAction(Request $req)
    {
        $order = new Order();
        $user = new User();
        $utils = new Utils();
        $filter = [
            "date_from" => date("Y-m-d H:i:s", strtotime("-1 months")),
            "date_to" => date("Y-m-d H:i:s", strtotime("now"))
        ];

        $orderRes = $order->findAllDetail($filter);

        $out = [
            'title' => 'Boozt test dashboard',
            'name' => 'Serj Senkin',
            'earnings' => $utils->getEarnings($orderRes),
            'countOrders' => $order->countAll($filter)['countOrders'],
            'countUsers' => $user->countAll($filter)['countUsers'],
            'grapthData' => json_encode($utils->convertDataForGraph($orderRes)),
        ];

        echo $this->twig->render('home.html.twig', $out);
    }
}
