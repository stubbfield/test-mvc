<?php
namespace App\Controller;

use App\Model\Order;
use App\Model\User;
use App\Lib\Utils;
use App\Lib\Request;
use App\Lib\Response;

class Ajax extends \App\Lib\Controller
{
    public function getOrdersByDateAction(Request $req)
    {
        $order = new Order();
        $user = new User();
        $utils = new Utils();
        $filter = $req->getBody();
        $orderRes = $order->findAllDetail($filter);
        

        $out = [
            'title' => 'Boozt test dashboard',
            'name' => 'Serj Senkin',
            'earnings' => $utils->getEarnings($orderRes),
            'countOrders' => $order->countAll($filter)['countOrders'],
            'countUsers' => $user->countAll($filter)['countUsers'],
            'grapthData' => json_encode($utils->convertDataForGraph($orderRes)),
        ];

        (new Response)->toJSON($out);
    }
}
