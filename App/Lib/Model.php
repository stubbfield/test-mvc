<?php

namespace App\Lib;

/**
 *  Base model interface
 */
abstract class Model
{
    protected $db;
    protected $dbOrder = 'boozt.order';
    protected $dbUser = 'boozt.user';
    protected $dbOrderItem = 'boozt.order_item';

    public function __construct()
    {
        $this->db = new Db();
    }
}
