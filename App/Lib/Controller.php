<?php

namespace App\Lib;

use \Twig\Environment;
use \Twig\Loader\FilesystemLoader;

/**
 *  Application Controller
 */

abstract class Controller
{
    protected $loader;
    protected $twig;

    public function __construct()
    {
        $this->loader = new FilesystemLoader('App/View');
        $this->twig = new Environment($this->loader);
    }
}
