<?php

namespace App\Lib;

/**
 * Utils
 */

class Utils
{

    public function getEarnings($orderItems)
    {
        $sum = array_sum(array_column($orderItems, 'price'));

        return $this->formatMoney($sum);
    }

    public function formatMoney($number)
    {
        return '$' . number_format($number, 2, '.', ',');
    }

    public function convertDataForGraph($data)
    {
        $arrData = [];
        $arrJs = [];
        foreach ($data as $order) {
            $orderDay = (new \DateTime($order['purchase_date']))->format('Y-m-d');

            if (!isset($arrData[$order['user_name']][$orderDay])) {
                $arrData[ $order['user_name'] ][$orderDay] = 0;
            }

            $arrData[ $order['user_name'] ][$orderDay] += $order['price'];
        }

        foreach ($arrData as $name => $dates) {
            $dateField = $name.'_date_data';
            $arrJs['xs'][$name] = $dateField;

            $rowDates = array_keys($dates);
            array_unshift($rowDates, $dateField);

            $rowValues = array_values($dates);
            array_unshift($rowValues, $name);

            $arrJs['columns'][] = $rowDates;
            $arrJs['columns'][] = $rowValues;
        }

        return $arrJs;
    }
}
