<?php

namespace App\Lib;

use App\Config;

/**
 * Get PDO to project
 */
class Db extends \PDO
{

    /**
     * Connect to DB by /App/Config
     *
     * @return array
     */
    public function __construct()
    {
        $aDriverOptions[\PDO::MYSQL_ATTR_INIT_COMMAND] = 'SET NAMES UTF8';
        parent::__construct(
            'mysql:host=' . Config::DB_HOST . ';dbname=' . Config::DB_NAME . ';',
            Config::DB_USER,
            Config::DB_PWD,
            $aDriverOptions
        );
        $this->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
    }
}
