<?php

namespace App;

/**
 *  Application configuration
 */
class Config
{

    /**
     * Database host
     *
     * @var string
     */
    const DB_HOST = '127.0.0.1';

    /**
     * Database name
     *
     * @var string
     */
    const DB_NAME = 'boozt';

    /**
     * Database user
     *
     * @var string
     */
    const DB_USER = 'user';

    /**
     * Database password
     *
     * @var string
     */
    const DB_PWD = '617043';

    /**
     * Show or hide error messages on screen
     *
     * @var boolean
     */
    const SHOW_ERRORS = true;

    /**
     * Show or hide error messages on screen
     *
     * @var boolean
     */
    const LOG_PATH = __DIR__ . '/logs';
}
