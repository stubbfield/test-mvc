# README #

MVC project.

### How do I get set up? ###

* clone
* setup config in "/App/config.php"
* create database
* do sql in "/App/migrations/"
* done

* for test setup url in "/tests/acceptance.suite.yml" and start with "php vendor/bin/codecept run --steps"